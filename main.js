$(document).ready(function () {
    let daysIndexArr = [], monthIndex, currentDate = new Date(), currentYear, currentMonth,
        monthArray = ["JAN", "FEB", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"];
    $("#year, #month").on("change", AdjustDays);
    // SHOW CALENDER
    function showCalendar(month, year) {
        $(".calender_table > tbody > tr > td").empty().css({backgroundColor:"white"});
        currentYear = year;
        monthIndex = month;
        daysIndexArr = []; let k = 0;
        currentMonth = monthArray[month];
        $(".current_my").html(currentMonth + "-" + year);
        for (i = 1; i <= new Date(year, (month + 1), 0).getDate(); i++) {
            daysIndexArr.push(new Date(year, month, i).getDay());
        }
        for (let tableCol = 0; tableCol < 6; tableCol++) {
            for (let tableRow = 0; tableRow < 7; tableRow++) {
                if (tableRow == daysIndexArr[k]) {
                    k++;
                    if (k == currentDate.getDate() && month == currentDate.getMonth() && year == currentDate.getFullYear()) {
                        $(".calender_table > tbody > tr").eq(tableCol).children().eq(tableRow).html(k).css({ backgroundColor: "rgb(206, 113, 32)" });
                    } else if (k == $('#day :selected').val() && month == $('#month :selected').index() && year == $('#year :selected').text()) {
                            $(".calender_table > tbody > tr").eq(tableCol).children().eq(tableRow).html(k).css({ backgroundColor: "white" });
                            $(".calender_table > tbody > tr").eq(tableCol).children().eq(tableRow).html(k).css({ backgroundColor: "rgb(240, 166, 101)" });
                        }
                        else {
                            $(".calender_table > tbody > tr").eq(tableCol).children().eq(tableRow).html(k).css({ backgroundColor: "white" });
                        }
                }
            }
        }
        $("tr").eq(5).children("td").eq(0).text().trim() == "" ? $("tr").eq(5).hide() : $("tr").eq(5).show()
        $("tr").eq(6).children("td").eq(0).text().trim() == "" ? $("tr").eq(6).hide() : $("tr").eq(6).show()
    }
    //  PREV BUTTON
    $(document).on("click", ".previous", function () {
        if (monthIndex <= 0) {
            monthIndex = 11;
            currentYear--;
        } else {
            monthIndex--;
        }
        //console.log(monthIndex, monthArray[monthIndex]);
        showCalendar(monthIndex, currentYear);
    })
    // NEXT BUTTON
    $(document).on("click", ".next", function () {
        if (monthIndex >= 11) {
            monthIndex = 0;
            currentYear++;
        } else {
            monthIndex++;
        }
        
        showCalendar(monthIndex, currentYear);
    })
    // TODAY BUTTON
    $(document).on("click", ".today_button", function () {
        showCalendar(currentDate.getMonth(),currentDate.getFullYear() );
    })
    $(".today_button").trigger("click");
    // FIND DATE BUTTON
    $(document).on("click", ".find_button", function () {
        showCalendar($('#month :selected').index(), $('#year :selected').text());
    });
    // DROPDOWN FUNCTIONS
    for (var y = 1970; y <= 2070; y++) {
        $("#year").append(`<option value=${y}>${y}</option>`);
    }
    monthArray.forEach((month, key) => {
        $("#month").append(`<option value=${key}>${month}</option>`);
    })
    AdjustDays();
    function AdjustDays() {
        $("#day").empty();
        for (var d = 1; d <= new Date($("#year").val(), (parseInt($("#month").val()) + 1), 0).getDate(); d++) {
            $("#day").append(`<option value=${d}>${d}</option>`);
        }
    }
});



